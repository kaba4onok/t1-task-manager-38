package ru.t1.rleonov.tm.exception.entity;

public final class EntityNotFoundException extends AbstractEntityNotFoundException {

    public EntityNotFoundException() {
        super("Error! Record not found...");
    }

}
