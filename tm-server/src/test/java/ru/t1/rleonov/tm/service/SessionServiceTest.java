package ru.t1.rleonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.ISessionRepository;
import ru.t1.rleonov.tm.api.service.ISessionService;
import ru.t1.rleonov.tm.comparator.CreatedComparator;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.exception.entity.EntityNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.model.Session;
import ru.t1.rleonov.tm.repository.SessionRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.t1.rleonov.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public class SessionServiceTest {

    /*@NotNull
    private final ISessionRepository repository = new SessionRepository();

    @NotNull
    private final ISessionService service = new SessionService(repository);

    @Before
    public void setUp() {
        repository.set(SESSIONS);
    }

    @After
    public void reset() {
        repository.clear();
    }

    @Test
    public void getSize() {
        @NotNull final String userId = SESSION1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(""));
        Assert.assertEquals(USER1_SESSIONS.size(), service.getSize(userId));
    }

    @Test
    public void add() {
        @NotNull final String userId = SESSION1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, SESSION1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add("", SESSION1));
        Assert.assertNull(service.add(userId, null));
        Assert.assertEquals(SESSION1, service.add(SESSION1));
    }

    @Test
    public void findAll() {
        @NotNull final String userId = SESSION1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll((String) null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        Assert.assertEquals(USER1_SESSIONS, service.findAll(userId));
    }

    @Test
    public void findAllComparator() {
        @NotNull final String userId = SESSION1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(null, Sort.BY_CREATED));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll("", Sort.BY_CREATED));
        @NotNull final List<Session> sortedSessions = new ArrayList<>(USER1_SESSIONS);
        sortedSessions.sort(CreatedComparator.INSTANCE);
        Assert.assertEquals(sortedSessions, service.findAll(userId, Sort.BY_CREATED));
    }

    @Test
    public void existsById() {
        @NotNull final String userId = SESSION1.getUserId();
        @NotNull final String id = SESSION1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, ""));
        Assert.assertTrue(service.existsById(userId, id));
    }

    @Test
    public void findOneById() {
        @NotNull final String userId = SESSION1.getUserId();
        @NotNull final String id = SESSION1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, ""));
        Assert.assertEquals(SESSION1, service.findOneById(userId, id));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final String userId = SESSION1.getUserId();
        @NotNull final Integer index = SESSIONS.indexOf(SESSION1);
        @NotNull final Integer badIndex = SESSIONS.size() + 1;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null, index));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex("", index));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, badIndex));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, -1));
        Assert.assertEquals(SESSION1, service.findOneByIndex(userId, index));
    }
    @Test
    public void remove() {
        @NotNull final String userId = SESSION1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(null, SESSION1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove("", SESSION1));
        Assert.assertNull(service.remove(userId, null));
        Assert.assertEquals(SESSION1, service.remove(SESSION1));
    }

    @Test
    public void removeById() {
        @NotNull final String userId = SESSION1.getUserId();
        @NotNull final String id = SESSION1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById("0", id));
        Assert.assertEquals(SESSION1, service.removeById(userId, id));
    }

    @Test
    public void removeByIndex() {
        @NotNull final String userId = SESSION1.getUserId();
        @NotNull final Integer index = SESSIONS.indexOf(SESSION1);
        @NotNull final Integer badIndex = SESSIONS.size() + 1;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(null, index));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex("", index));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, badIndex));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, -1));
        Assert.assertEquals(SESSION1, service.removeByIndex(userId, index));
    }

    @Test
    public void clear() {
        @NotNull final String userId = SESSION1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll((String) null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        service.clear(userId);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(userId));
    }*/

}
