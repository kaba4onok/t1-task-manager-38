package ru.t1.rleonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.api.service.IProjectService;
import ru.t1.rleonov.tm.comparator.CreatedComparator;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.exception.entity.EntityNotFoundException;
import ru.t1.rleonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.repository.ProjectRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    /*@NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository);

    @Before
    public void setUp() {
        repository.set(ALL_PROJECTS);
    }

    @After
    public void reset() {
        repository.clear();
    }

    @Test
    public void create() {
        @NotNull final String userId = NULL_DESC_PROJECT.getUserId();
        @NotNull final String name = NULL_DESC_PROJECT.getName();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, name));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", name));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertEquals(name, repository.create(userId, name).getName());
    }

    @Test
    public void createWithDesc() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final String name = USER1_PROJECT1.getName();
        @NotNull final String desc = USER1_PROJECT1.getDescription();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, name, desc));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, "", desc));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, name, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, name, ""));
        Assert.assertEquals(name, repository.create(userId, name, desc).getName());
    }

    @Test
    public void updateById() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final String id = USER1_PROJECT1.getId();
        @NotNull final String name = USER1_PROJECT1.getName();
        @NotNull final String desc = USER1_PROJECT1.getDescription();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(null, id, name, desc));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById("", id, name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(userId, null, name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(userId, "", name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, id, null, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, id, "", desc));
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                service.updateById(WRONG_USERID_PROJECT.getUserId(), id, name, desc));
        Assert.assertEquals(USER1_PROJECT2.getName(),
                service.updateById(userId, id, USER1_PROJECT2.getName(), desc).getName());
    }

    @Test
    public void updateByIndex() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final Integer index = USER1_PROJECTS.indexOf(USER1_PROJECT1);
        @NotNull final Integer badIndex = USER1_PROJECTS.size() + 1;
        @NotNull final String name = USER1_PROJECT1.getName();
        @NotNull final String desc = USER1_PROJECT1.getDescription();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(null, index, name, desc));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex("", index, name, desc));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(userId, null, name, desc));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(userId, badIndex, name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(userId, index, null, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(userId, index, "", desc));
        Assert.assertEquals(USER1_PROJECT2.getName(),
                service.updateByIndex(userId, index, USER1_PROJECT2.getName(), desc).getName());
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final String id = USER1_PROJECT1.getId();
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusById(null, id, status));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusById("", id, status));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeProjectStatusById(userId, null, status));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeProjectStatusById(userId, "", status));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeProjectStatusById(userId, id, null));
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                service.changeProjectStatusById(WRONG_USERID_PROJECT.getUserId(), id, status));
        Assert.assertEquals(status, service.changeProjectStatusById(userId, id, status).getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final Integer index = USER1_PROJECTS.indexOf(USER1_PROJECT1);
        @NotNull final Integer badIndex = USER1_PROJECTS.size() + 1;
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusByIndex(null, index, status));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusByIndex("", index, status));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeProjectStatusByIndex(userId, null, status));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeProjectStatusByIndex(userId, badIndex, status));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeProjectStatusByIndex(userId, index, null));
        Assert.assertEquals(status, service.changeProjectStatusByIndex(userId, index, status).getStatus());
    }

    @Test
    public void getSize() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(""));
        Assert.assertEquals(USER1_PROJECTS.size(), service.getSize(userId));
    }

    @Test
    public void add() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, USER1_PROJECT1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add("", USER1_PROJECT1));
        Assert.assertNull(service.add(userId, null));
        Assert.assertEquals(USER1_PROJECT1, service.add(USER1_PROJECT1));
    }

    @Test
    public void findAll() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll((String) null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        Assert.assertEquals(USER1_PROJECTS, service.findAll(userId));
    }

    @Test
    public void findAllComparator() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(null, Sort.BY_CREATED));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll("", Sort.BY_CREATED));
        @NotNull final List<Project> sortedProjects = new ArrayList<>(USER1_PROJECTS);
        sortedProjects.sort(CreatedComparator.INSTANCE);
        Assert.assertEquals(sortedProjects, service.findAll(userId, Sort.BY_CREATED));
    }

    @Test
    public void existsById() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final String id = USER1_PROJECT1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, ""));
        Assert.assertTrue(service.existsById(userId, id));
    }

    @Test
    public void findOneById() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final String id = USER1_PROJECT1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, ""));
        Assert.assertEquals(USER1_PROJECT1, service.findOneById(userId, id));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final Integer index = USER1_PROJECTS.indexOf(USER1_PROJECT1);
        @NotNull final Integer badIndex = USER1_PROJECTS.size() + 1;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null, index));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex("", index));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, badIndex));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, -1));
        Assert.assertEquals(USER1_PROJECT1, service.findOneByIndex(userId, index));
    }
    @Test
    public void remove() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(null, USER1_PROJECT1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove("", USER1_PROJECT1));
        Assert.assertNull(service.remove(userId, null));
        Assert.assertEquals(USER1_PROJECT1, service.remove(USER1_PROJECT1));
    }

    @Test
    public void removeById() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final String id = USER1_PROJECT1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById("0", id));
        Assert.assertEquals(USER1_PROJECT1, service.removeById(userId, id));
    }

    @Test
    public void removeByIndex() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        @NotNull final Integer index = USER1_PROJECTS.indexOf(USER1_PROJECT1);
        @NotNull final Integer badIndex = USER1_PROJECTS.size() + 1;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(null, index));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex("", index));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, badIndex));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, -1));
        Assert.assertEquals(USER1_PROJECT1, service.removeByIndex(userId, index));
    }

    @Test
    public void clear() {
        @NotNull final String userId = USER1_PROJECT1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll((String) null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        service.clear(userId);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(userId));
    }*/

}
