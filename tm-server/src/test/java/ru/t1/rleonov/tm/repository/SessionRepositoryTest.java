package ru.t1.rleonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.ISessionRepository;
import ru.t1.rleonov.tm.marker.UnitCategory;
import static ru.t1.rleonov.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    /*@Test
    public void getSize() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(SESSIONS);
        Assert.assertEquals(SESSIONS.size(), repository.getSize());
    }

    @Test
    public void add() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(SESSION1);
        Assert.assertEquals(SESSION1, repository.findOneByIndex(0));
    }

    @Test
    public void findAll() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(SESSIONS);
        Assert.assertEquals(SESSIONS, repository.findAll());
    }

    @Test
    public void existsById() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(SESSIONS);
        @NotNull final String session_id = SESSION1.getId();
        Assert.assertNotNull(repository.findOneById(session_id));
    }

    @Test
    public void findOneById() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(SESSIONS);
        @NotNull final String session_id = SESSION1.getId();
        Assert.assertEquals(SESSION1, repository.findOneById(session_id));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(SESSIONS);
        @NotNull final Integer index = SESSIONS.indexOf(SESSION1);
        Assert.assertEquals(SESSION1, repository.findOneByIndex(index));
    }

    @Test
    public void remove() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(SESSIONS);
        repository.remove(SESSION1);
        Assert.assertEquals(SESSION2, repository.findOneByIndex(0));
    }

    @Test
    public void removeById() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(SESSIONS);
        @NotNull final String session_id = SESSION1.getId();
        repository.removeById(session_id);
        Assert.assertEquals(SESSION2, repository.findOneByIndex(0));
    }

    @Test
    public void removeByIndex() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(SESSIONS);
        @NotNull final Integer index = SESSIONS.indexOf(SESSION1);
        repository.removeByIndex(index);
        Assert.assertEquals(SESSION2, repository.findOneByIndex(0));
    }*/

}
