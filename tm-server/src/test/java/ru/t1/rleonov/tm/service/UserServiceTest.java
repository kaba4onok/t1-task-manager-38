package ru.t1.rleonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.repository.IUserRepository;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.IUserService;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.exception.entity.UserNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.exception.user.EmailExistsException;
import ru.t1.rleonov.tm.exception.user.LoginExistsException;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.repository.ProjectRepository;
import ru.t1.rleonov.tm.repository.TaskRepository;
import ru.t1.rleonov.tm.repository.UserRepository;
import ru.t1.rleonov.tm.util.HashUtil;
import java.util.Collections;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;
import static ru.t1.rleonov.tm.constant.TaskTestData.ALL_TASKS;
import static ru.t1.rleonov.tm.constant.TaskTestData.USER1_TASKS;
import static ru.t1.rleonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    /*@NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService service = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Before
    public void setUp() {
        userRepository.set(USERS);
        projectRepository.set(ALL_PROJECTS);
        taskRepository.set(ALL_TASKS);
    }

    @After
    public void reset() {
        userRepository.clear();
        projectRepository.clear();
        taskRepository.clear();
    }

    @Test
    public void create() {
        @NotNull final String login = USER1.getLogin();
        @NotNull final String password = USER1.getLogin();
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(null, password));
        Assert.assertThrows(LoginEmptyException.class, () -> service.create("", password));
        Assert.assertThrows(LoginExistsException.class, () -> service.create(login, password));
        userRepository.clear();
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(login, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(login, ""));
        Assert.assertEquals(USER1.getLogin(), service.create(login, password).getLogin());
    }

    @Test
    public void createWithEmail() {
        @NotNull final String login = USER1.getLogin();
        @NotNull final String password = USER1.getLogin();
        @NotNull final String email = USER1.getEmail();
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(null, password, email));
        Assert.assertThrows(LoginEmptyException.class, () -> service.create("", password, email));
        Assert.assertThrows(LoginExistsException.class, () -> service.create(login, password, email));
        userRepository.clear();
        Assert.assertThrows(EmailEmptyException.class, () -> service.create(login, password, (String) null));
        Assert.assertThrows(EmailEmptyException.class, () -> service.create(login, password, ""));
        service.create(login, password, email);
        Assert.assertThrows(EmailExistsException.class, () -> service.create(USER2.getLogin(), password, email));
        userRepository.clear();
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(login, null, email));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(login, "", email));
        Assert.assertEquals(USER1.getLogin(), service.create(login, password, email).getLogin());
    }

    @Test
    public void createWithRole() {
        @NotNull final String login = USER1.getLogin();
        @NotNull final String password = USER1.getLogin();
        @NotNull final Role role = Role.ADMIN;
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(null, password, role));
        Assert.assertThrows(LoginEmptyException.class, () -> service.create("", password, role));
        Assert.assertThrows(LoginExistsException.class, () -> service.create(login, password, role));
        userRepository.clear();
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(login, null, role));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(login, "", role));
        Assert.assertThrows(RoleEmptyException.class, () -> service.create(login, password, (Role) null));
        Assert.assertEquals(Role.ADMIN, service.create(login, password, role).getRole());
    }

    @Test
    public void findByLogin() {
        @NotNull final String login = USER1.getLogin();
        Assert.assertThrows(LoginEmptyException.class, () -> service.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> service.findByLogin(""));
        Assert.assertEquals(USER1, service.findByLogin(login));
    }

    @Test
    public void findByEmail() {
        @NotNull final String email = USER1.getEmail();
        Assert.assertThrows(EmailEmptyException.class, () -> service.findByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> service.findByEmail(""));
        Assert.assertEquals(USER1, service.findByEmail(email));
    }

    @Test
    public void removeByLogin() {
        @NotNull final String login = USER1.getLogin();
        Assert.assertThrows(LoginEmptyException.class, () -> service.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> service.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> service.removeByLogin(NONAME));
        Assert.assertEquals(USER1, service.removeByLogin(login));
    }

    @Test
    public void removeByEmail() {
        @NotNull final String email = USER1.getEmail();
        Assert.assertThrows(EmailEmptyException.class, () -> service.removeByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> service.removeByEmail(""));
        Assert.assertThrows(UserNotFoundException.class, () -> service.removeByEmail(NONAME));
        Assert.assertEquals(USER1, service.removeByEmail(email));
    }

    @Test
    public void remove() {
        @NotNull final String userId = USER1.getId();
        Assert.assertEquals(USER1, userRepository.findByLogin(USER1.getLogin()));
        Assert.assertEquals(USER1_PROJECTS, projectRepository.findAll(userId));
        Assert.assertEquals(USER1_TASKS, taskRepository.findAll(userId));
        Assert.assertNull(service.remove(null));
        service.remove(USER1);
        Assert.assertEquals(Collections.EMPTY_LIST, projectRepository.findAll(userId));
        Assert.assertEquals(Collections.EMPTY_LIST, taskRepository.findAll(userId));
    }

    @Test
    public void setPassword() {
        @NotNull final String id = USER1.getId();
        @NotNull final String password = NONAME;
        Assert.assertThrows(IdEmptyException.class, () -> service.setPassword(null, password));
        Assert.assertThrows(IdEmptyException.class, () -> service.setPassword("", password));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.setPassword(id, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.setPassword(id, ""));
        Assert.assertThrows(UserNotFoundException.class, () -> service.setPassword(NONAME, password));
        Assert.assertEquals(HashUtil.salt(propertyService, NONAME), service.setPassword(id, password).getPasswordHash());
    }

    @Test
    public void updateUser() {
        @NotNull final String id = USER1.getId();
        @NotNull final String fname = USER2.getFirstName();
        @NotNull final String lname = USER2.getLastName();
        @NotNull final String mname = USER2.getMiddleName();
        Assert.assertThrows(IdEmptyException.class, () -> service.updateUser(null, fname, lname, mname));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateUser("", fname, lname, mname));
        Assert.assertThrows(UserNotFoundException.class, () -> service.updateUser(NONAME, fname, lname, mname));
        service.updateUser(id, fname, lname, mname);
        Assert.assertEquals(fname, service.findByLogin(USER1.getLogin()).getFirstName());
        Assert.assertEquals(lname, service.findByLogin(USER1.getLogin()).getLastName());
        Assert.assertEquals(mname, service.findByLogin(USER1.getLogin()).getMiddleName());
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(service.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(service.isLoginExist(null));
        Assert.assertFalse(service.isLoginExist(""));
        Assert.assertFalse(service.isLoginExist(NONAME));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(service.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(service.isEmailExist(null));
        Assert.assertFalse(service.isEmailExist(""));
        Assert.assertFalse(service.isEmailExist(NONAME));
    }

    @Test
    public void lockUserByLogin() {
        @NotNull final String login = USER1.getLogin();
        Assert.assertFalse(service.findByLogin(login).getLocked());
        Assert.assertThrows(LoginEmptyException.class, () -> service.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> service.lockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> service.lockUserByLogin(NONAME));
        Assert.assertTrue(service.lockUserByLogin(login).getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull final String login = USER1.getLogin();
        service.findByLogin(login).setLocked(true);
        Assert.assertTrue(service.findByLogin(login).getLocked());
        Assert.assertThrows(LoginEmptyException.class, () -> service.unlockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> service.unlockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> service.unlockUserByLogin(NONAME));
        Assert.assertFalse(service.unlockUserByLogin(login).getLocked());
    }

    @Test
    public void getSize() {
        Assert.assertEquals(USERS.size(), service.getSize());
    }

    @Test
    public void add() {
        userRepository.clear();
        Assert.assertEquals(Collections.EMPTY_LIST, service.findAll());
        Assert.assertNull(service.add((User) null));
        Assert.assertEquals(USER1, service.add(USER1));
    }

    @Test
    public void addCollection() {
        userRepository.clear();
        Assert.assertEquals(Collections.EMPTY_LIST, service.findAll());
        Assert.assertEquals(Collections.EMPTY_LIST, service.add(Collections.EMPTY_LIST));
        Assert.assertEquals(USERS, service.add(USERS));
    }

    @Test
    public void set() {
        userRepository.clear();
        Assert.assertEquals(Collections.EMPTY_LIST, service.findAll());
        Assert.assertEquals(Collections.EMPTY_LIST, service.set(Collections.EMPTY_LIST));
        Assert.assertEquals(USERS, service.set(USERS));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USERS, service.findAll());
    }

    @Test
    public void existsById() {
        @NotNull final String id = USER1.getId();
        Assert.assertTrue(service.existsById(id));
        Assert.assertFalse(service.existsById(NONAME));
    }

    @Test
    public void findOneById() {
        @NotNull final String id = USER1.getId();
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(""));
        Assert.assertEquals(USER1, service.findOneById(id));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final Integer index = USERS.indexOf(USER1);
        @NotNull final Integer badIndex = USERS.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(badIndex));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(-1));
        Assert.assertEquals(USER1, service.findOneByIndex(index));
    }

    @Test
    public void removeById() {
        @NotNull final String id = USER1.getId();
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(""));
        Assert.assertEquals(USER1, service.removeById(id));
    }

    @Test
    public void removeByIndex() {
        @NotNull final Integer index = USERS.indexOf(USER1);
        @NotNull final Integer badIndex = USERS.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(badIndex));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(-1));
        Assert.assertEquals(USER1, service.removeByIndex(index));
    }

    @Test
    public void clear() {
        service.clear();
        Assert.assertEquals(Collections.emptyList(), userRepository.findAll());
    }*/

}
