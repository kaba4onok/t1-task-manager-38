package ru.t1.rleonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.IRepository;
import ru.t1.rleonov.tm.comparator.CreatedComparator;
import ru.t1.rleonov.tm.comparator.StatusComparator;
import ru.t1.rleonov.tm.model.AbstractModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    @NotNull
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull List<M> result = new ArrayList<>();
        for (M model: models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final Comparator comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s ORDER BY %s;",
                getTableName(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }
    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Override
    @SneakyThrows
    public int count() {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M remove(@NotNull final M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("TRUNCATE TABLE %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.execute(sql);
        }
    }

}
