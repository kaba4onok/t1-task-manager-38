package ru.t1.rleonov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.ISessionRepository;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.ISessionService;
import ru.t1.rleonov.tm.model.Session;
import ru.t1.rleonov.tm.repository.SessionRepository;
import java.sql.Connection;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    public ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}
