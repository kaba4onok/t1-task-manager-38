package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.Session;
import ru.t1.rleonov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login,
                  @Nullable String password,
                  @Nullable String email);

    @NotNull
    String login(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    Session validateToken(@Nullable String token);

    void logout(@Nullable final Session session);

}
