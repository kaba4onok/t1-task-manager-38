package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId);

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId);

    Project removeProjectById(
            @Nullable String userId,
            @Nullable String projectId);

}
