package ru.t1.rleonov.tm.api.service;

import ru.t1.rleonov.tm.api.repository.IRepository;

public interface IService<M> extends IRepository<M> {
}
