package ru.t1.rleonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Project;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public String getTableName() {
        return "tm.project";
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project fetch(@NotNull ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(Status.toStatus(row.getString("status")));
        project.setCreated(row.getTimestamp("created"));
        project.setUserId(row.getString("user_id"));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, name, description, status, created, user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?);",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getStatus().toString());
            statement.setTimestamp(5, new Timestamp(project.getCreated().getTime()));
            statement.setString(6, project.getUserId());
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull String userId, @NotNull Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET name = ?, description = ?, status = ? WHERE id = ? AND user_id = ?;", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.setString(5, project.getUserId());
            statement.executeUpdate();
        }
        return project;
    }

}
