package ru.t1.rleonov.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.User;
import java.sql.ResultSet;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    String getTableName();

    @NotNull
    User fetch(@NotNull ResultSet row);

    @NotNull
    @Override
    User add(@NotNull User user);

    @NotNull
    abstract User update(@NotNull User user);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    Boolean isLoginExist(@NotNull String login);

    Boolean isEmailExist(@NotNull String email);

}
