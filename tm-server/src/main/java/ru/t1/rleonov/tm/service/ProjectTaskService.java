package ru.t1.rleonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.rleonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.rleonov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.rleonov.tm.exception.field.TaskIdEmptyException;
import ru.t1.rleonov.tm.exception.field.UserIdEmptyException;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.repository.TaskRepository;
import java.sql.Connection;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    public ITaskRepository getTaskRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    public IProjectService projectService;

    @NotNull
    public ITaskService taskService;

    public ProjectTaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.connectionService = connectionService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = getTaskRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = getTaskRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public Project removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        for (@NotNull final Task task : tasks) taskService.removeById(userId, task.getId());
        return projectService.removeById(userId, projectId);
    }

}
