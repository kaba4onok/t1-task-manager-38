package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getDbLogin();

    @NotNull
    String getDbPassword();

    @NotNull
    String getDbUrl();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getSessionKey();

    @NotNull
    public Integer getSessionTimeout();

}
