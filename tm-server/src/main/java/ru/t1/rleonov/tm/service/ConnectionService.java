package ru.t1.rleonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String login = propertyService.getDbLogin();
        @NotNull final String password = propertyService.getDbPassword();
        @NotNull final String url = propertyService.getDbUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, login, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
