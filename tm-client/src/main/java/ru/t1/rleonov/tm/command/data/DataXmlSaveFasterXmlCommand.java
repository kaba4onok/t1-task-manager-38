package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataXmlFasterXmlRequest;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-xml-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file using fasterxml.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerSaveDataXmlFasterXmlRequest request = new ServerSaveDataXmlFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
