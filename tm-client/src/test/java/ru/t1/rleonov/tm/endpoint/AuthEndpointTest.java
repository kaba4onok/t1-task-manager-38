package ru.t1.rleonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.dto.request.UserLoginRequest;
import ru.t1.rleonov.tm.dto.request.UserLogoutRequest;
import ru.t1.rleonov.tm.marker.IntegrationCategory;
import ru.t1.rleonov.tm.service.PropertyService;
import static ru.t1.rleonov.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    /*@NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Test
    public void login() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(USER1.getLogin());
        request.setPassword(USER1.getPasswordHash());
        Assert.assertNotNull(authEndpoint.login(request).getToken());
    }

    @Test
    public void logout() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(USER2.getLogin());
        request.setPassword(USER2.getPasswordHash());
        @Nullable final String token = authEndpoint.login(request).getToken();
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(token);
        Assert.assertNotNull(authEndpoint.logout(logoutRequest));
    }*/

}
